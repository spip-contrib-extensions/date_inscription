<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Declaration des tables principales
 *
 * @param array $tables_principales
 * @return array
 */
function date_inscription_declarer_tables_principales($tables_principales) {
	$tables_principales['spip_auteurs']['field']['date_inscription'] = "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL";
	return $tables_principales;
}

/**
 * Upgrade des tables
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 * @return void
 */
function date_inscription_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];
	$maj['create'] = [
		['maj_tables', ['spip_auteurs']],
	];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Desinstallation
 *
 * @param string $nom_meta_base_version
 */
function date_inscription_vider_tables($nom_meta_base_version) {
	sql_alter('TABLE spip_auteurs DROP date_inscription');
	effacer_meta($nom_meta_base_version);
}
