<?php

return [

	// D
	'date_inscription_description' => 'Un champ supplémentaire pour la table auteurs : date_inscription',
	'date_inscription_nom' => 'Date d’inscription',
	'date_inscription_slogan' => 'Une date d’inscription pour les auteurs',
];